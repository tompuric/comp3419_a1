package com.tw0v.comp3419_a1;

import com.tw0v.comp3419_a1.graphics.Graphics;
import com.tw0v.comp3419_a1.input.Input;
import com.tw0v.comp3419_a1.io.FileIO;
import com.tw0v.comp3419_a1.screen.Screen;
import com.tw0v.comp3419_a1.sound.Audio;

public interface Game {
	public Input getInput();
	public FileIO getFileIO();
	public Graphics getGraphics();
	public Audio getAudio();
	public void setScreen(Screen screen);
	public Screen getCurrentScreen();
	public Screen getStartScreen();
}
