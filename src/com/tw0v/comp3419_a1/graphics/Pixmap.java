package com.tw0v.comp3419_a1.graphics;

import com.tw0v.comp3419_a1.graphics.Graphics.PixmapFormat;

public interface Pixmap {
	public int getWidth();
	public int getHeight();
	public PixmapFormat getFormat();
	public void dispose();
}
