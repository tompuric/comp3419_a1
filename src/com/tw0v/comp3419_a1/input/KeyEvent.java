package com.tw0v.comp3419_a1.input;

public class KeyEvent {
	public static final int KEY_DOWN = 0;
	public static final int KEY_UP = 1;
	public int type;
	public int keyCode;
	public char keyChar;
}
