package com.tw0v.comp3419_a1;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;

public class ImageReader {
	
	private static final int WHITE = 0xFFFFFFFF;
	private static final int BLACK = 0xFF000000;
	public static final int SIZE = 4;

	public Bitmap src = null;
	private int nodes = 0;
	
	List<Pixel> regions;
	int linked[][];
	Pixel pixels[][];
	static int map[][] = new int[SIZE][SIZE];
	float dstF[] = new float[8];
	float srcF[] = new float[8];
	
	public Bitmap modify()
	{
		linked = new int[src.getWidth()][src.getHeight()];
		pixels = new Pixel[src.getWidth()][src.getHeight()];

		int w = (int) (src.getWidth()/1.5);
		int h = (int) (src.getHeight()/1.5);
		Bitmap src_original = null;
		
		// Grey Scale Image and Perform Binary Operation
		try {
			src = greyScale();
		} catch (Exception e) {
			System.out.println("Please make sure the photo is portrait");
			System.exit(0);
		}
		
		
		// Threshold and removing image borders
		src = threshold(5, 0.9f);
		src_original = Bitmap.createBitmap(src);
		// Set src_original to the black and white image
		src = binarisation();
		
		// Perform blob extraction to get the border (in order to find the corners)
		src = blobExtraction();
		
		 
		// Calculate corners
		Point ptSrc[] = {cornerTLExtraction(),
							cornerTRExtraction(),
							cornerBRExtraction(),
							cornerBLExtraction()};
		
		Point ptDst[] = {new Point(0, 0),
							new Point(w, 0),
							new Point(w, h),
							new Point(0, h)};
		
		
		// Remove the largest blob (which is the border)
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = 0; x < src.getWidth(); x++) {
				if (linked[x][y] == 0) continue;
				if (linked[x][y] >= regions.size()) continue;
				linked[x][y] = regions.get(linked[x][y]).getMin();
				
				if (linked[x][y] == getLargestBlob(regions)) {
					src_original.setPixel(x, y, WHITE);
				}
			}
		}
		
		// Perform the perspective transform function
		src = transform(ptSrc, ptDst, src_original);
		
		// Grab the image of the new perspective transformed image
		w = (int) (srcF[6] - srcF[0]);
		h = (int) (srcF[7] - srcF[1]);
		try {
			src = Bitmap.createBitmap(src, (int) srcF[0], (int) srcF[1], w, h);
		} catch (Exception e) {
			System.out.println("Invalid image");
			System.exit(0);
		}
		// Test the number of each block
		
		Bitmap new_src = null;
		for (int y = 0; y < SIZE; y++) {
			for (int x = 0; x < SIZE; x++) {
				new_src = Bitmap.createBitmap(src, x*(w/SIZE), y*(h/SIZE), (w/SIZE), (h/SIZE));
				map[x][y] = testAI(new_src);
			}
		}
		
		return src;
	}
	
	private int testAI(Bitmap src) {
		int w = src.getWidth();
		int h = src.getHeight();
		boolean canSearch = true;
		int y = h/2, x = 0;
		int count = 0;
		int numBlack = 0;
		for (x = 0; x < w; x++) {
			if (src.getPixel(x, y) == WHITE) {
				canSearch = true;
				continue;
			}
			
			if (canSearch) count++;
			canSearch = false;
		}
		for (y = 0; y < h; y++) {
			for (x = 0; x < w; x++) {
				if (src.getPixel(x, y) == BLACK) numBlack++;
			}
		}
		
		// If there's clutter, ignore the block
		if (numBlack < 3) {
			return 0;
		}
		// Otherwise determine the character
		else {
			// Ensure there's only two 1 nodes, otherwise set to a wall
			if (count == 1 && nodes > 1)
				return 2;
			else {
				if (count == 1) nodes++;
				if (count > 2) count = 2;
				return count;
			}
				
		}
	}
	
	public Bitmap transform(Point[] ptSrc, Point[] ptDst, Bitmap src_original) {
		Matrix matrix = new Matrix();
		
		dstF[0] = ptDst[0].x;
		dstF[1] = ptDst[0].y;
		dstF[2] = ptDst[1].x;
		dstF[3] = ptDst[1].y;
		dstF[4] = ptDst[3].x;
		dstF[5] = ptDst[3].y;
		dstF[6] = ptDst[2].x;
		dstF[7] = ptDst[2].y;
		
		srcF[0] = ptSrc[0].x;
		srcF[1] = ptSrc[0].y;
		srcF[2] = ptSrc[1].x;
		srcF[3] = ptSrc[1].y;
		srcF[4] = ptSrc[3].x;
		srcF[5] = ptSrc[3].y;
		srcF[6] = ptSrc[2].x;
		srcF[7] = ptSrc[2].y;
		
		matrix.setPolyToPoly(srcF, 0, dstF, 0, srcF.length >> 1);
		matrix.mapVectors(srcF);
		
		return Bitmap.createBitmap(src_original, 0, 0, src_original.getWidth(), src_original.getHeight(), matrix, true);
	}
	
	private Bitmap greyScale() {
		Bitmap image = Bitmap.createBitmap(src);
		for(int y = 0; y < image.getHeight(); y++)
		{
			for(int x = 0; x < image.getWidth(); x++)
			{
				image.setPixel(x, y, grey(image.getPixel(x, y)));
			}
		}
		return image;
	}
	
	private Bitmap binarisation() {
		Bitmap image = Bitmap.createBitmap(src);
		for(int y = 0; y < image.getHeight(); y++)
		{
			for(int x = 0; x < image.getWidth(); x++)
			{
				image.setPixel(x, y, bin(image.getPixel(x, y)));
			}
		}
		return image;
	}

	private Bitmap threshold(int K, float threshold) {
		if (K > 8) K = 8;
		if (K < 1) K = 1;
		Bitmap image = Bitmap.createBitmap(src);
		for(int row = 0; row < src.getHeight(); row++)
		{
			for(int column = 0; column < src.getWidth(); column++)
			{
				int a = 0;
				int c = 0;
				
				int colour = (src.getPixel(column, row) >> 16 & 0x0000FF);
				
				for (int x = column - K; x <= column + K; x++) {
					if (x < 0 || x >= src.getWidth()) continue;
					
					for (int y = row - K; y <= row + K; y++) {
						if (y < 0 || y >= src.getHeight()) continue;
						
						a++;
						c += src.getPixel(x, y) >> 16 & 0x0000FF;
					}
				}
				c = c/a;
				if (colour < c*threshold) 
					c = 0x00;
				else
					c = 0xFF;
				image.setPixel(column, row, 0xFF << 24 | c << 16 | c << 8 | c);
			}
		}
		return image;
	}
	
	private Bitmap blobExtraction() {
		int vx[] = {-1, -1,  0,  1};
		int vy[] = { 0, -1, -1, -1};
		
		int blobs = 1;
		regions = new ArrayList<Pixel>();
		regions.add(new Pixel());
		
		Bitmap image = Bitmap.createBitmap(src);
		
		// FIRST RUN
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = 0; x < src.getWidth(); x++) {
				pixels[x][y] = new Pixel();
				if ((src.getPixel(x, y) & WHITE) == BLACK) continue;
				
				for (int i = 0; i < vx.length; i++) {
					if (x + vx[i] < 0 || x + vx[i] >= src.getWidth() || y + vy[i] < 0 || y + vy[i] >= src.getHeight()) continue;
					if (linked[x + vx[i]][y + vy[i]] == 0) continue;
					pixels[x][y].add(pixels[x + vx[i]][y + vy[i]]);					
				}
				
				if (pixels[x][y].connections.size() == 0) {
					regions.add(new Pixel());
					linked[x][y] = blobs;
					pixels[x][y].add(linked[x][y]);
					blobs++;
				}
				else {
					linked[x][y] = pixels[x][y].getMin();
					regions.get(linked[x][y]).add(pixels[x][y]);
				}
			}
		}
		
		// Perform Unions of clusters
		for (int j = 0; j < regions.size(); j++) {
			for (int i = 0; i < regions.get(j).connections.size(); i++) {
				regions.get(regions.get(j).connections.get(i)).add(regions.get(j));
			}
		}
		
		// SECOND RUN
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = 0; x < src.getWidth(); x++) {
				if (linked[x][y] == 0) continue;
				if (linked[x][y] >= regions.size()) continue;
				linked[x][y] = regions.get(linked[x][y]).getMin();
				
				int a = 0;
				if (linked[x][y] == getLargestBlob(regions))
					a = WHITE;
				else
					a = BLACK;
				image.setPixel(x, y, a);
			}
		}
		return image;
	}
	
	private int getLargestBlob(List<Pixel> regions) {
		int max = 0, region = 0;
		for (int i = 1; i < regions.size(); i++) {
			if (regions.get(i).size() > max) {
				max = regions.get(i).size();
				region = i;
			}
		}
		return region;
	}


	public Point cornerTLExtraction() {
		int pixel[][] = new int[src.getWidth()][src.getHeight()];
		int xPos = src.getWidth()/2;
		int yPos = src.getHeight()/2;
		
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = 0; x < src.getWidth(); x++) {
				if ((src.getPixel(x, y) & WHITE) == BLACK) continue;
				pixel[x][y] = x + y;
				if (pixel[x][y] < xPos+yPos) {
					xPos = x;
					yPos = y;
				}
			}
		}
		return new Point(xPos, yPos);
	}
	
	public Point cornerTRExtraction() {
		int pixel[][] = new int[src.getWidth()][src.getHeight()];
		int xPos = 0, yPos = 0;
		int min = src.getWidth();
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = src.getWidth() - 1; x >= 0; x--) {
				if ((src.getPixel(x, y) & WHITE) == BLACK) continue;
				pixel[x][y] = (src.getWidth() - x) + y;
				if (pixel[x][y] < min) {
					min = pixel[x][y];
					xPos = x;
					yPos = y;
				}
			}
		}
		return new Point(xPos, yPos);
	}
	
	public Point cornerBLExtraction() {
		int pixel[][] = new int[src.getWidth()][src.getHeight()];
		int xPos = 0, yPos = 0;
		int min = src.getWidth();
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = src.getWidth() - 1; x >= 0; x--) {
				if ((src.getPixel(x, y) & WHITE) == BLACK) continue;
				pixel[x][y] = (src.getWidth() - x) + y;
				if (pixel[x][y] > min) {
					min = pixel[x][y];
					xPos = x;
					yPos = y;
				}
			}
		}
		return new Point(xPos, yPos);
	}
	
	public Point cornerBRExtraction() {
		int pixel[][] = new int[src.getWidth()][src.getHeight()];
		int xPos = src.getWidth()/2;
		int yPos = src.getHeight()/2;
		
		for (int y = 0; y < src.getHeight(); y++) {
			for (int x = 0; x < src.getWidth(); x++) {
				if ((src.getPixel(x, y) & WHITE) == BLACK) continue;
				pixel[x][y] = x + y;
				if (pixel[x][y] > xPos+yPos) {
					xPos = x;
					yPos = y;
				}
			}
		}
		return new Point(xPos, yPos);
	}
	
	public void closing() {
		src = erosion();
		src = dilation();
	}
	
	
	public Bitmap dilation() {
		
		Bitmap img = Bitmap.createBitmap(src);
		
		boolean[][] struct ={ {false, false, false},
	  			  {false, true, false},
	  			  {false, false, false} };
		
		for(int column = 0; column < src.getWidth(); column++)
		{
			for(int row = 0; row < src.getHeight(); row++)
			{
				int c = src.getPixel(column, row);
				
				if (c == BLACK) continue;
				
				for (int x = column - 1; x <= column + 1; x++) {
					if (x < 0 || x >= src.getWidth()) continue;
					
					for (int y = row - 1; y <= row + 1; y++) {
						if (y < 0 || y >= src.getHeight()) continue;
						int newC = src.getPixel(x, y);
						if (struct[x - (column - 1)][y - (row - 1)] && newC == BLACK) {
							c = BLACK;
						}
					}
				}
				
				img.setPixel(column, row, c);
				
			}
		}
		return img;
	}
	
	public Bitmap erosion() {
		
		Bitmap img = Bitmap.createBitmap(src);
		
		
		boolean[][] struct ={ {false, false, false},
				  			  {false, true, false},
				  			  {false, false, false} };
		
		int totalStruct = 0;
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				if (struct[x][y]) totalStruct++;
			}
		}
				
		for(int row = 0; row < src.getHeight(); row++)
		
		{
			for(int column = 0; column < src.getWidth(); column++)
			{
				int newC = BLACK;
				
				int a = 0;
				
				for (int x = column - 1; x <= column + 1; x++) {
					if (x < 0 || x >= src.getWidth()) continue;
					
					for (int y = row - 1; y <= row + 1; y++) {
						if (y < 0 || y >= src.getHeight()) continue;
						newC = src.getPixel(x, y);
						if (struct[x - (column - 1)][y - (row - 1)] && newC == BLACK) {
							a++;
						}
						
					}
					
				}
				
				if (a < totalStruct)
					img.setPixel(column, row, WHITE);
				else
					img.setPixel(column, row, BLACK);

			}
		}
		return img;
	}
	

	
	public int grey (int c) {
		int r = c >> 16 & 0x0000FF;
		int g = c >> 8 & 0x0000FF;
		int b = c & 0x0000FF;
		int col = (int)(0.212671*r + 0.715160*g + 0.072169*b);
		return 0xFF << 24 | col << 16 | col << 8 | col;
	}
	
	public int bin (int c) {
		int r = c >> 16 & 0x0000FF;
		int g = c >> 8 & 0x0000FF;
		int b = c & 0x0000FF;
		int col = (int)(0.212671*r + 0.715160*g + 0.072169*b);
		if (col < 127 ) col = 0xFF;
		else col = 0x00;
		return 0xFF << 24 | col << 16 | col << 8 | col;
	}
	
}