package com.tw0v.comp3419_a1;

import com.tw0v.comp3419_a1.graphics.Graphics;
import com.tw0v.comp3419_a1.world.World;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

class AndroidFastRenderView extends SurfaceView implements Runnable {
	AndroidGame game;
	World world = new World();
	Bitmap framebuffer;
	Thread renderThread = null;
	SurfaceHolder holder;
	volatile boolean running = false;
	Rect dstRect = new Rect(0, 0, 600, 600);
	Graphics g;
	int x= 0, y = 0;
	int vx = 1, vy = 1;
	
	int width;
	int height;
	
	long ticks = 0, frames = 0;
	
	public AndroidFastRenderView(Context context) {
		super(context);
		this.holder = getHolder();
	}
	
	public AndroidFastRenderView(AndroidGame game, Bitmap framebuffer) {
		super(game);
		this.game = game;
		this.framebuffer = framebuffer;
		this.holder = getHolder();
		this.g = game.graphics;
		width = framebuffer.getWidth();
		height = framebuffer.getHeight();
		
	}
	
	public void resume() {
		running = true;
		renderThread = new Thread(this);
		renderThread.start();
		world.init(game);
	}
	
	public void pause() {
		running = false;
		while(true) {
			try {
				renderThread.join();
				break;
			} catch (InterruptedException e) {
				
			}
		}
	}

	@Override
	public void run() {
		
		while(running) {
			if (!holder.getSurface().isValid()) continue;
			tick();
			render();
			if (game.getInput().isTouchDown(0))
				System.out.println(game.getInput().isTouchDown(0));
		}
	}

	private void tick() {
		ticks++;
		world.tick();
	}

	private void render() {
		frames++;
		Canvas canvas = holder.lockCanvas();
		canvas.getClipBounds(dstRect);
		g.clear(Color.WHITE);
		
		world.render(g);
		canvas.drawBitmap(framebuffer, 0, 0, null);
		//canvas.drawBi
		holder.unlockCanvasAndPost(canvas);
		
	}
	
}