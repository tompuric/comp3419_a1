package com.tw0v.comp3419_a1.sound;

public interface Sound {
	public void play(float volume);
	public void dispose();
}
