package com.tw0v.comp3419_a1.sound;


public interface Audio {
	public Music newMusic(String filename);
	public Sound newSound(String filename);
}
