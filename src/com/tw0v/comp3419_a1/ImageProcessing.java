package com.tw0v.comp3419_a1;

import android.graphics.Bitmap;

public class ImageProcessing {
	public ImageReader ir = new ImageReader();
	public Bitmap image;
	int width;
	int height;
	
	public ImageProcessing(Bitmap image) {
		this.image = image;
		width = image.getWidth();
		height = image.getHeight();
		ir.src = Bitmap.createBitmap(image);
	}
	
	public Bitmap tick() {
		return ir.modify();
	}
}
