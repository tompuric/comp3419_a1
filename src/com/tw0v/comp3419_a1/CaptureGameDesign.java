package com.tw0v.comp3419_a1;

import com.tw0v.androidtest1.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class CaptureGameDesign extends Activity {
	private static final int ACTION_TAKE_PHOTO_BACK = 1;
	private ImageView mImageView;
	private Bitmap mImageBitmap;
	private int width = 0, height = 0;
	int[] pixels;
	ImageProcessing process;
	Intent game = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mImageView = (ImageView) findViewById(R.id.imageView1);
		Button bPicture = (Button) findViewById(R.id.tutorial1);
		Button pPicture = (Button) findViewById(R.id.processImage);
		mImageBitmap = null;
		
		// Set up intent to load game
		try {
			game = new Intent(this, Class.forName("com.tw0v.comp3419_a1.AndroidGame"));
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		bPicture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dispatchTakePictureIntent(ACTION_TAKE_PHOTO_BACK);
			}
			
		});
		
		pPicture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (width == 0 || height == 0) return;
				process = new ImageProcessing(mImageBitmap);
				mImageBitmap = process.tick();
				mImageView.setImageBitmap(mImageBitmap);
				mImageView.setVisibility(View.VISIBLE);
				try {
					// load game
					startActivity(game);
				} catch (Exception e) {
					
				}
			}
			
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}

	private void dispatchTakePictureIntent(int actionCode) {
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		
		startActivityForResult(cameraIntent, actionCode);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case ACTION_TAKE_PHOTO_BACK:
				if (resultCode == RESULT_OK) handleBigCameraPhoto(data);
				break;
			default:
				break;
		}
	}

	private void handleBigCameraPhoto(Intent data) {
		setPic(data);
	}
	
	private void setPic(Intent data) {
		Bundle extras = data.getExtras();
		mImageBitmap = (Bitmap) extras.get("data");
		mImageView.setImageBitmap(mImageBitmap);
		mImageView.setVisibility(View.VISIBLE);
		width = mImageBitmap.getWidth();
		height = mImageBitmap.getHeight();
	}
	
}
