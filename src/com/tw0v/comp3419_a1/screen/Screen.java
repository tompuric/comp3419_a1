package com.tw0v.comp3419_a1.screen;

import com.tw0v.comp3419_a1.Game;

public abstract class Screen {
	protected final Game game;
	
	public Screen(Game game) {
		this.game = game;
	}
	
	public abstract void update(float deltaTime);
	public abstract void present(float deltaTime);
	public abstract void pause();
	public abstract void resume();
	public abstract void dispose();
}
