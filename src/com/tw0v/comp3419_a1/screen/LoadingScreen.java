package com.tw0v.comp3419_a1.screen;

import com.tw0v.comp3419_a1.Game;

public class LoadingScreen extends Screen {

	public LoadingScreen(Game game) {
		super(game);

	}

	@Override
	public void update(float deltaTime) {
		//Graphics g = game.getGraphics();
		game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void present(float deltaTime) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
