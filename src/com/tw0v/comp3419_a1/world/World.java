package com.tw0v.comp3419_a1.world;

import java.util.ArrayList;

import com.tw0v.comp3419_a1.AndroidGame;
import com.tw0v.comp3419_a1.entity.Entity;
import com.tw0v.comp3419_a1.entity.Goal;
import com.tw0v.comp3419_a1.entity.Player;
import com.tw0v.comp3419_a1.entity.Rectangle;
import com.tw0v.comp3419_a1.entity.Wall;
import com.tw0v.comp3419_a1.graphics.Graphics;

public class World {
	public AndroidGame game;
	public Player player = new Player(0, 0);
	public int map[][];
	public ArrayList<Entity> entities = new ArrayList<Entity>();
	public int width = 720, height = 720;
	public boolean won = false;
	
	boolean lost = false;
	
	public void init(AndroidGame game) {
		this.game = game;
		map = AndroidGame.map;
		player.init(this);
		if (map != null) {
			loadWorld();
		}
	}
	
	private void loadWorld() {
		for (int y = 0; y < map.length; y++) {
			for (int x = 0; x < map.length; x++) {
				switch (map[x][y]) {
					case 1:
						if (entities.contains(player)) add(new Goal(x*(width/4), y*(height/4)));
						else {
							player = new Player(x*(width/4), y*(height/4));
							add(player);
						}
						break;
					case 2:
						add(new Wall(x*(width/4), y*(height/4)));
						break;
					default:
						break;
				}
			}
		}
	}
	
	public void tick() {
		if (Math.abs(game.getInput().getAccelX()) < 3)
			player.vx = 0;
		if (game.getInput().getAccelX() <= -3) player.vx = 1;
		if (game.getInput().getAccelX() >= 3) player.vx = -1;
		
		if (Math.abs(game.getInput().getAccelY()) < 3)
			player.vy = 0;
		if (game.getInput().getAccelY() <= -3) player.vy = -1;
		if (game.getInput().getAccelY() >= 3) player.vy = 1;
		
		player.tick(); 
		for (Entity e : entities) {
			e.tick();
		}
		
		if (won) System.exit(0);
	}
	
	public void render(Graphics g) {
		player.render(g);
		for (Entity e : entities) {
			e.render(g);
		}
	}
	
	public void add(Entity e) {
		if (e instanceof Player) {
			player = (Player) e;
			player.x = 0;
			player.y = 0;
		}
		entities.add(e);
		e.removed = false;
		e.init(this);
	}
	
	public void remove(Entity e) {
		if (e instanceof Player)
			lost = true;
		entities.remove(e);
	}
	
	public ArrayList<Entity> getEntities(Rectangle r) {
		ArrayList<Entity> result = new ArrayList<Entity>();
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (r.intersects(e.rect)) {
				result.add(e);
			}
		}
		return result;
	}
	
}
