package com.tw0v.comp3419_a1;

import java.util.ArrayList;
import java.util.List;

public class Pixel {
	public List<Integer> connections = new ArrayList<Integer>();
	
	public Pixel() {
		
	}
	
	
	public void add(int area) {
		if (connections.contains(area)) return;
		connections.add(area);
		//System.out.println(area);
	}
	
	public int size() {
		return connections.size();
	}
	
	public void add(Pixel pixel) {
		for (int i = 0; i < pixel.connections.size(); i++) {
			if (connections.contains(pixel.connections.get(i))) continue;
			
			add(pixel.connections.get(i));
		}
	}
	
	public int getMin() {
		if (connections.size() == 0) return 0;
		
		int minimum = connections.get(0);
		for (int i = 0; i < connections.size(); i++) {
			if (connections.get(i) < minimum)
				minimum = connections.get(i);
		}
		
		return minimum;
	}
	
}
