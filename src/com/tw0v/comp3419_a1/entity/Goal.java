package com.tw0v.comp3419_a1.entity;

import android.graphics.Color;

import com.tw0v.comp3419_a1.graphics.Graphics;

public class Goal extends Entity {
	public Goal(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g) {
		g.drawRect(x, y, width, height, Color.RED);
	}
	
	@Override
	public boolean canPass() {
		return true;
	}
	
	public void touchedBy(Entity e) {
		if (e instanceof Player) {
			if (e.x == x && e.y == y) {
				world.won = true;
			}
		}
	}
}
