package com.tw0v.comp3419_a1.entity;

public class Rectangle {
	public int x;
	public int y;
	public int w;
	public int h;
	
	public Rectangle(int x, int y, int w, int h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean intersects(Rectangle r) {
		boolean intersectsX = false;
		boolean intersectsY = false;
		
		if ((r.x >= x && r.x < x + w - 1) || (r.x + r.w - 1 > x && r.x + r.w <= x + w - 1)) intersectsX = true;
		if ((r.y >= y && r.y < y + h - 1) || (r.y + r.h - 1 > y && r.y + r.h <= y + h - 1)) intersectsY = true;
		if (intersectsX && intersectsY) return true;
		else return false;
	}
	
	public boolean contains(Rectangle r) {
		boolean intersectsX = false;
		boolean intersectsY = false;
		
		if ((r.x == x && r.x == x + w) || (r.x + r.w == x && r.x + r.w == x + w)) intersectsX = true;
		if ((r.y == y && r.y == y + h) || (r.y + r.h == y && r.y + r.h == y + h)) intersectsY = true;
		if (intersectsX && intersectsY) return true;
		else return false;
	}
}
