package com.tw0v.comp3419_a1.entity;

import android.graphics.Color;

import com.tw0v.comp3419_a1.AndroidGame;
import com.tw0v.comp3419_a1.graphics.Graphics;

public class Player extends Entity {
	public int vx = 0;
	public int vy = 0;
	
	public Player(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void tick() {
		move(vx, 0);
		move(0, vy);
		
		if (x < 0) x = 0;
		if (x + width > AndroidGame.WIDTH) x = AndroidGame.WIDTH - width;
		if (y < 0) y = 0;
		if (y + height > AndroidGame.HEIGHT) y = AndroidGame.HEIGHT - height;
	}
	
	public void render(Graphics g) {
		g.drawRect(x, y, width, height, Color.BLUE);
	}
}
