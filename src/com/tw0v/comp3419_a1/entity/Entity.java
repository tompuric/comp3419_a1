package com.tw0v.comp3419_a1.entity;

import java.util.ArrayList;

import com.tw0v.comp3419_a1.ImageReader;
import com.tw0v.comp3419_a1.graphics.Graphics;
import com.tw0v.comp3419_a1.world.World;

import android.graphics.Rect;

public class Entity {
	public World world;
	public Rect r;
	public int x;
	public int y;
	public int width;
	public int height;
	public boolean removed;
	public Rectangle rect;
	
	public void init(World world) {
		this.world = world;
		width = 720/ImageReader.SIZE;
		height = 720/ImageReader.SIZE;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void move(int vx, int vy) {
		ArrayList<Entity> entities = world.getEntities(new Rectangle(x + vx, y + vy, width, height));
		for (int i = 0; i < entities.size(); i++) {
			
			Entity e = entities.get(i);
			if (e == this) continue;
			if (!e.canPass()) return;
			e.touchedBy(this);
		}
		
		
		x += vx;
		y += vy;
		
		rect.setLocation(x, y);
	}
	
	public void touchedBy(Entity e) {
		
	}
	
	public boolean canPass() {
		return false;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g) {
		
	}
}
