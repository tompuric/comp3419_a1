package com.tw0v.comp3419_a1.entity;

import android.graphics.Color;

import com.tw0v.comp3419_a1.graphics.Graphics;

public class Wall extends Entity {
	public Wall(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics g) {
		g.drawRect(x, y, width, height, Color.BLACK);
	}
}
